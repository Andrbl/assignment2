# README #

# Assignment2
Artificial intelligence 2020  

# Member
	Andreas Blakli                     andrbl           andrbl@stud.ntnu.no	
	
assignment2Task1.py contains task 1.1 and 1.2  
assignment2Task2.py contains task 2.1  

# Note!
To get the program to run it's next task each window with the plotted clusters must be exited (press X on the window).  
Commenting out elbowMethod() and silhouetteMethod() in each k-clustering function speeds up the program, but obviously it won't show the elbow and silhouette graphs.  
Due to the size of the fashion and stl10 datasets the program is slow.  
After using the yellowbrick.cluster library for making the silhouette graphs, the colors of k-clusters got changed to black and white, I was not able to fix this. 
The workaround to get the colors back is to comment out "import yellowbrick.cluster as yb" and the elbowMethod(), silhouetteMethod() functions.  

# Sources
Iris dataset: https://archive.ics.uci.edu/ml/datasets/iris  
Fashion MNIST dataset: https://github.com/zalandoresearch/fashion-mnist  
STL-10 dataset: https://cs.stanford.edu/~acoates/stl10/  
Image used in task 2.1: https://pixabay.com/photos/bird-woodpecker-pileated-woodpecker-5702542/