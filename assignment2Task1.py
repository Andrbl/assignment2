import yellowbrick.cluster as yb
import matplotlib.pyplot as plt
import matplotlib
import sys
import sklearn
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
from sklearn.preprocessing import StandardScaler
from sklearn import preprocessing
import pandas as pd
import numpy as np 
from mnist import MNIST
from sklearn.decomposition import PCA
from scipy.io import loadmat
from matplotlib.image import imread


##-----Elbow and Silhouette-----##
# Task 1.2

def elbowMethod(df):                    # Function for creating elbow graphs
        distortion = []
        K = range(1,11)
        for k in K:
            kmElbow = KMeans(n_clusters=k)
            kmElbow.fit(df)
            distortion.append(kmElbow.inertia_)
        plt.plot(K, distortion, "bx-")
        plt.xlabel("k-cluster")
        plt.ylabel("Distortion")
        plt.title("Elbow method")
        plt.show()


def silhouetteMethod(df):               # Function for creating silhouette "graphs"
    fig, ax = plt.subplots(2, 2)
    K = range(2,11)
    for k in K:
        kmSil = KMeans(n_clusters=k)
        visualizer = yb.SilhouetteVisualizer(kmSil, colors="bold")
        visualizer.fit(df)
        plt.title("Silhouette method: num K: "+ str(k))
        plt.show()
    visualizer.poof()


##-----Datasets k-clustering-----##
# Task 1.1

def syntheticData():                                                                            # Creating a synthetic dataset
    X, y = make_blobs(n_samples=300, n_features=2, centers=5, cluster_std=0.5, shuffle=True, random_state=0)
                                                                                                # 5 Clusters
    elbowMethod(X)                             # Calling function for display of elbowMethod
    silhouetteMethod(X)                        # Calling function for display of silhouetteMethod
    km = KMeans(n_clusters=5)
    km.fit(X)
    centroids = km.cluster_centers_
    print(centroids)
    plt.scatter(X[:, 0], X[:, 1], c="white", marker="o", edgecolor="black", s=50)               # Plotting data onto graph
    plt.scatter(centroids[:, 0], centroids[:, 1], c='red', s=50)
    plt.show()


def irisData():                                 # Function for creating k-clusters from iris datasets
    path = "iris.data"
    df = pd.read_csv(path, usecols=[0,1])       # add usecols=[0,1,2,3] for petal
    km = KMeans(n_clusters=3).fit(df)
    print(df)
    elbowMethod(df)                             # Calling function for display of elbowMethod
    silhouetteMethod(df)                        # Calling function for display of silhouetteMethod
    centroids = km.cluster_centers_
    print(centroids)
    plt.scatter(df.iloc[:, 0], df.iloc[:, 1], c= km.labels_.astype(float), cmap="Spectral", s=50, alpha=0.9)     #For sepal.length and sepal.width
    #plt.scatter(df.iloc[:, 2], df.iloc[:, 3], c= km.labels_.astype(float), s=50, alpha=0.5)    #For petal.length and petal.width
    plt.scatter(centroids[:, 0], centroids[:, 1], c='red', s=50)
    plt.show()


def fashionDataset():                           # Function for creating k-clusters from the fashion dataset
    print("starting fashion")
    mndata = MNIST("./fashion")
    mndata.gz = True
    xTrain, yTrain = mndata.load_training()

    X = StandardScaler().fit_transform(xTrain)
    pcaConv= PCA(5)
    xConv = pcaConv.fit_transform(X)
    xConv.shape

    km = KMeans(n_clusters=10).fit(xConv, yTrain)
    print(xConv)

    elbowMethod(xConv)                             # Calling function for display of elbowMethod
    silhouetteMethod(xConv)                        # Calling function for display of silhouetteMethod
    
    centroids = km.cluster_centers_
    print(centroids)    

    plt.scatter(centroids[:, 0], centroids[:, 1], c='red', s=100, alpha= 0.9)
    plt.scatter(x=xConv[:, 0], y=xConv[:,1], c= km.labels_.astype(float), s=10, alpha=0.5)
    plt.show()

    
def stl10Dataset():                                 # Function for creating k-clusters from the stl10 dataset
    with open("train_X.bin", 'rb') as f:
        everything = np.fromfile(f, dtype=np.uint8)
        xTrain = np.reshape(everything, (-1, 3, 96, 96))
    with open("train_y.bin", 'rb') as f:
        yTrain = np.fromfile(f, dtype=np.uint8)
    #X = StandardScaler().fit_transform(xTrain)
    xTrainShape, nx, ny, nz = xTrain.shape
    xTrainReShape = xTrain.reshape((xTrainShape,nx*ny*nz))
    pcaConv= PCA(3)
    xConv = pcaConv.fit_transform(xTrainReShape)
    #xConv.shape
    km = KMeans(n_clusters=10).fit(xTrainReShape, yTrain)

    elbowMethod(xConv)                             # Calling function for display of elbowMethod
    silhouetteMethod(xConv)                        # Calling function for display of silhouetteMethod

    centroids = km.cluster_centers_
    print(centroids) 

    plt.scatter(centroids[:, 0], centroids[:, 1], c='red', s=100, alpha= 0.9)
    plt.scatter(x=xConv[:, 0], y=xConv[:, 1], c= km.labels_.astype(float), s=10, alpha=0.5)
    plt.show()


##---------Run all functions---------##

syntheticData()
irisData()
fashionDataset()
stl10Dataset()